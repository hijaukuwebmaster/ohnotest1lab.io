import axios from 'axios';
import cheerio from 'cheerio';
import qs from 'querystring';
import { URL } from 'url';
import { waitUntilReady } from './scheduler';
import { getHTML } from './utils';

const defaultHeaders = {
  'User-Agent':
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36',
  Accept: '*/*',
  'Accept-Encoding': 'gzip, deflate, br',
  Connection: 'keep-alive',
};

export const getProductsByCatalog = async (
  url: string,
  brandId?: string
): Promise<string[] | undefined> => {
  const hasQuery = url.match(/\?(.+)/);
  const pageHTML = await getHTML(url);

  if (!pageHTML) return;

  const $ = cheerio.load(pageHTML);

  const canonicalURL =
    $('[rel="canonical"]')
      ?.attr('href')
      ?.replace('https://www.zalora.co.id', '') ?? url;

  // @ts-ignore
  const [matchBrand, targetBrand] = pageHTML.match(/targetBrand: (.+),/);

  let userParams: { [key: string]: any } = {};

  const hasSegment = pageHTML.match(/segment: '(\w+)'/);

  if (hasSegment) {
    const [, segment] = hasSegment;
    userParams['segment'] = segment;
    userParams['gender'] = segment;
  }

  const hasTargetCategoryId = pageHTML.match(/targetCategoryId: '(\w+)'/);

  if (hasTargetCategoryId) {
    const [, targetCategoryId] = hasTargetCategoryId;
    userParams['category_id'] = targetCategoryId;
  }

  if (matchBrand) {
    userParams['brand'] = JSON.parse(targetBrand)?.['id_catalog_brand'] ?? '';
  }

  if (hasQuery) {
    const [, queryparams] = hasQuery;
    const qp = qs.parse(queryparams);

    userParams = {
      ...userParams,
      ...qp,
    };
    if (qp['category_id'] && !Array.isArray(qp['category_id'])) {
      userParams['category_id'] = qp['category_id'].split('--');
    }
  } else {
    const catId = findCategoryId(pageHTML);
    userParams['category_id'] = catId;
  }

  let currentOffset = 0;
  const urls = [];

  while (true) {
    try {
      const params: {
        [key: string]: any;
      } = {
        url: canonicalURL,
        sort: 'popularity',
        dir: 'desc',
        offset: currentOffset,
        limit: 96,
        category_id: '',
        range: '',
        condition: '',
        occasion: '',
        material_composition: '',
        pattern: '',
        campaign_categoryId: '',
        color: '',
        sizesystem: '',
        brand: brandId ?? '',
        age_group: '',
        gender: '',
        price: '',
        normalized_sell_through: '',
        segment: '',
        special_price: false,
        all_products: false,
        new_products: false,
        top_sellers: false,
        catalogtype: 'Main',
        campaign: '',
        discount: '',
        age: '',
        mp: '',
        or: '',
        shipment_type: '',
        exs: '',
        lang: 'id',
        is_brunei: false,
        search_suggest: false,
        elevate_ids: '',
        user_id: '',
        enable_visual_sort: true,
        enable_filter_ads: true,
        user_query: '',
        is_multiple_source: true,
        compact_catalog_desktop: false,
        name_search: false,
        solr7_support: true,
        pick_for_you: false,
        learn_to_sort_catalog: false,
        ...userParams,
      };

      for (const key in params) {
        if (params[key] === '') {
          delete params[key];
        }
      }

      const query = qs.stringify(params);

      await waitUntilReady();
      const response = await axios.get(
        `https://www.zalora.co.id/_c/v1/desktop/list_catalog_full?${query}`,
        {
          headers: defaultHeaders,
        }
      );

      if (response.data.response.docs.length === 0) break;

      currentOffset = currentOffset + response.data.response.docs.length;
      urls.push(
        ...response.data.response.docs.map(
          (d: string) => `https://www.zalora.co.id/${d.link}`
        )
      );

      console.log('Mendapat', urls.length, 'produk dari', url);
    } catch (error) {
      break;
    }
  }

  return urls;
};

type BrandSegment = {
  count: number;
  display_name: string;
  name: string;
};

type BrandCategory = {
  category_urls: {
    [key: string]: string;
  };
  default_name: string;
  depth: string;
  id_catalog_category: string;
  is_searchable: string;
  name: string;
  name_en: string;
  primary: string;
  segment_urls: string;
  segments: string;
  url_key: string;
};

type BrandSubcategory = {
  category_urls: {
    [key: string]: string;
  };
  children: {
    [key: string]: BrandCategory;
  };
};

export const findBrandSegments = async (
  url: string
): Promise<{
  kind: 'segment';
  data: {
    segments_refine: BrandSegment[];
  };
}> => {
  const params = {
    url,
    sort: 'popularity',
    dir: 'desc',
    offset: '0',
    limit: '96',
    brand: '349',
    special_price: 'false',
    all_products: 'false',
    new_products: 'false',
    top_sellers: 'false',
    catalogtype: 'Main',
    lang: 'id',
    is_brunei: 'false',
    search_suggest: 'false',
    enable_visual_sort: 'true',
    enable_filter_ads: 'true',
    compact_catalog_desktop: 'false',
    name_search: 'false',
    solr7_support: 'true',
    pick_for_you: 'false',
    learn_to_sort_catalog: 'false',
    is_multiple_source: 'true',
  };

  const query = qs.stringify(params);

  await waitUntilReady();
  const response = await axios.get(
    `https://www.zalora.co.id/_c/v1/desktop/list_catalog_full?${query}`,
    {
      headers: defaultHeaders,
    }
  );

  return {
    kind: 'segment',
    data: {
      segments_refine: response.data.segments_refine as BrandSegment[],
    },
  };
};

export const findBrandCategories = async (
  brandId: string,
  segmentName: string
): Promise<{
  kind: 'category';
  data: {
    root_tree: BrandCategory[];
  };
}> => {
  const params = {
    url: `/${segmentName}`,
    sort: 'popularity',
    dir: 'desc',
    offset: '0',
    limit: '96',
    brand: brandId,
    special_price: 'false',
    all_products: 'false',
    new_products: 'false',
    top_sellers: 'false',
    catalogtype: 'Main',
    lang: 'id',
    is_brunei: 'false',
    search_suggest: 'false',
    enable_visual_sort: 'true',
    enable_filter_ads: 'true',
    compact_catalog_desktop: 'false',
    name_search: 'false',
    solr7_support: 'true',
    pick_for_you: 'false',
    learn_to_sort_catalog: 'false',
    is_multiple_source: 'true',
  };

  const query = qs.stringify(params);

  await waitUntilReady();
  const response = await axios.get(
    `https://www.zalora.co.id/_c/v1/desktop/list_catalog_full?${query}`,
    {
      headers: defaultHeaders,
    }
  );

  return {
    kind: 'category',
    data: {
      root_tree: response.data.root_tree as BrandCategory[],
    },
  };
};

export const findBrandSubcategories = async ({
  brandId,
  categoryUrl,
  category_id,
  segment,
}: {
  brandId: string;
  categoryUrl: string;
  category_id: string;
  segment: string;
}): Promise<{
  kind: 'subcategory';
  data: {
    root_tree: BrandCategory[];
    category_tree: BrandSubcategory;
  };
}> => {
  const params = {
    url: categoryUrl,
    sort: 'popularity',
    dir: 'desc',
    offset: '0',
    limit: '96',
    brand: brandId,
    category_id,
    segment,
    special_price: 'false',
    all_products: 'false',
    new_products: 'false',
    top_sellers: 'false',
    catalogtype: 'Main',
    lang: 'id',
    is_brunei: 'false',
    search_suggest: 'false',
    enable_visual_sort: 'true',
    enable_filter_ads: 'true',
    compact_catalog_desktop: 'false',
    name_search: 'false',
    solr7_support: 'true',
    pick_for_you: 'false',
    learn_to_sort_catalog: 'false',
    is_multiple_source: 'true',
  };

  const query = qs.stringify(params);

  await waitUntilReady();
  const response = await axios.get(
    `https://www.zalora.co.id/_c/v1/desktop/list_catalog_full?${query}`,
    {
      headers: defaultHeaders,
    }
  );

  return {
    kind: 'subcategory',
    data: {
      root_tree: response.data.root_tree as BrandCategory[],
      category_tree: response.data.category_tree as BrandSubcategory,
    },
  };
};

export const findBrandId = (html: string): string => {
  const $ = cheerio.load(html);
  const androidMetaUrl = $('meta[property="al:android:url"]')?.attr('content');

  if (androidMetaUrl) {
    const parsedUrl = new URL(androidMetaUrl);
    const query = qs.parse(parsedUrl.search.substr(1));

    if (query['brandIds[]']) {
      return query['brandIds[]'] as string;
    }
  }
  throw new Error('Gagal menemukan brand id');
};

export const findCategoryId = (html: string): string => {
  const $ = cheerio.load(html);
  const androidMetaUrl = $('meta[property="al:android:url"]')?.attr('content');

  if (androidMetaUrl) {
    const parsedUrl = new URL(androidMetaUrl);
    const query = qs.parse(parsedUrl.search.substr(1));

    if (query['categoryId']) {
      return query['categoryId'] as string;
    }
  }
  throw new Error('Gagal menemukan category id');
};

export const findURLBySKU = async (sku: string) => {
  const params = {
    url: '/catalog',
    q: sku,
    sort: 'popularity',
    dir: 'desc',
    offset: '0',
    limit: '96',
    special_price: 'false',
    all_products: 'false',
    new_products: 'false',
    top_sellers: 'false',
    catalogtype: 'Main',
    lang: 'id',
    is_brunei: 'false',
    sort_formula:
      'sum(product(0.1,score_simple_availability),product(0.0,score_novelty),product(0.9,score_product_boost),product(0.0,score_random),product(1.0,score_personalization))',
    rerank_formula:
      'sum(product(0.20,score_simple_availability),product(0,score_novelty),product(0.80,score_product_boost),product(0.0,score_random))',
    search_suggest: 'false',
    enable_visual_sort: 'false',
    enable_filter_ads: 'true',
    compact_catalog_desktop: 'false',
    name_search: 'false',
    solr7_support: 'false',
    pick_for_you: 'false',
    learn_to_sort_catalog: 'false',
    user_query: sku,
    is_multiple_source: 'true',
    enable_similar_term: 'true',
  };
  const query = qs.stringify(params);

  await waitUntilReady();
  const response = await axios.get(
    `https://www.zalora.co.id/_c/v1/desktop/list_catalog_full?${query}`,
    {
      headers: defaultHeaders,
    }
  );

  if (!response.data.response.docs[0]) {
    throw new Error('Hasil pencarian kosong');
  }
  return `https://www.zalora.co.id/${response.data.response.docs[0].link}`;
};

export const getZaloraLiteJSON = ($: cheerio.Root): any => {
  const script = $('script')
    .toArray()
    .find((el) => $(el).html()?.includes('utag_data'));

  const match = $(script)
    .html()
    ?.match(/{(.*)}/gms);

  if (match) {
    const sanitized = `${match[0]
      .toString()
      .replace(/\'/g, '"')
      .replace('document.title', '""')
      .replace(/\s+/gms, '')
      .replace(',}', '}')}`;

    const json = JSON.parse(sanitized);

    return json;
  } else {
    throw new Error('Gagal mendapatkan harga');
  }
};

export const getProductBreadcrumbs = ($: cheerio.Root) => {
  return $('.b-breadcrumbs li')
    .slice(1)
    .map((i, li) => {
      return $(li).text().trim();
    })
    .get();
};

export const getProductTitleAndBrand = (
  $: cheerio.Root
): {
  title: string;
  brand: string;
} => {
  const match = $('title')
    .text()
    .match(/Jual (.+) \|/i);

  if (match) {
    let [_, title] = match;
    title = title.replace(/ Original$/i, '');
    const brand = $('[itemprop="brand"]').text().trim();

    return {
      title,
      brand,
    };
  } else {
    const json = getZaloraLiteJSON($);
    const brand_property = json['Brand'] ?? json['Product_Brand'];

    return {
      title: json['Product_Name']?.[0] ?? json['Product_Name'] ?? '',
      brand: brand_property?.[0] ?? brand_property ?? '',
    };
  }
};

export const getProductImages = ($: cheerio.Root) => {
  const images: { [key: string]: string } = {};

  $('[data-image-product]').each((i, img) => {
    if (i < 8) {
      const index = i + 1;
      const imgURL = $(img)
        ?.attr('data-image-product')
        ?.replace(/(https.+)\)\//, '');

      if (imgURL) {
        images[`Foto Produk ${index}`] = imgURL;
      }
    }
  });

  return images;
};

export const getProductDetails = ($: cheerio.Root) => {
  const details: { [key: string]: string } = {};

  $('#productDetails table').find('br').replaceWith('\n');
  const kvs: { key: string; value: string }[] = $('#productDetails table')
    .find('tr')
    .map((i, tr) => {
      // Skip SKU (Simple)
      if (i === 0) return null

      const tds = $(tr).find('td');

      if (tds?.length > 0) {
        return {
          key: $(tds[0]).text().trim(),
          value: $(tds[1]).text().trim(),
        };
      }
      return null;
    })
    .get();

  for (const { key, value } of kvs) {
    if (key && value) {
      details[key] = value;
    }
  }

  return details;
};

export const getProductSizes = ($: cheerio.Root) => {
  $('#sizeDetails').find('br').replaceWith('\n');
  const measurement = $('#sizeDetails .size__measurement').text().trim();
  const attributes = $('#sizeDetails .size__attributes')
    .text()
    .trim()
    .split(/\r?\n/)
    .map((a) => a.trim())
    .join('\n');

  return {
    measurement,
    attributes,
  };
};

export const getProductPrices = ($: cheerio.Root) => {
  const afterDisc = $('[itemprop="price"]').attr('content');
  const beforeDisc = $('[name="selectedPrice"]').attr('value');

  if (!afterDisc || !beforeDisc) {
    const prices = getZaloraLiteJSON($);

    return {
      afterDisc:
        parseInt(prices['Product_Price']?.[0] ?? prices['Product_Price']) ?? 0,
      beforeDisc:
        parseInt(prices['Unit_Price']?.[0] ?? prices['Unit_Price']) ?? 0,
    };
  }

  return {
    afterDisc: afterDisc ? parseInt(afterDisc) : 0,
    beforeDisc: beforeDisc ? parseInt(beforeDisc) : 0,
  };
};

export const getProductDescription = ($: cheerio.Root) => {
  $('[itemprop="description"]').find('br').replaceWith('\n');
  const desc = $('[itemprop="description"]').text().trim();

  return desc;
};

export const getProductSKU = ($: cheerio.Root) => {
  return $('#configSku').attr('value');
};

export const getProductPriceVariants = (
  $: cheerio.Root
): {
  [key: string]: {
    price: string;
    special_price: string;
    price_with_currency: string;
    special_price_with_currency: string;
    saving_percentage: string;
    is_overseas: boolean;
  };
} => {
  const prices = $('script')
    .map((i, elm) => {
      const html = $(elm).html();
      if (html?.includes('priceStore')) {
        // @ts-ignore
        const [_, json] = html.match(
          /Zalora.jsStore\["priceStore-\w+"] = (.+);/
        );
        return JSON.parse(json)['prices'];
      }
    })
    .get();

  return prices.find(Boolean);
};
